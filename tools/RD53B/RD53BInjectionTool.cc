#include "RD53BInjectionTool.h"

#include <boost/version.hpp>

#if BOOST_VERSION >= 106400
#include <boost/integer/common_factor_rt.hpp>
#define LCM boost::integer::lcm
#else
#include <boost/math/common_factor_rt.hpp>
#define LCM boost::math::lcm
#endif

#include "../Utils/xtensor/xview.hpp"
#include "../Utils/xtensor/xindex_view.hpp"
#include "../Utils/xtensor/xio.hpp"

#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TSystem.h>
#include <TGaxis.h>

using namespace RD53BEventDecoding;

namespace RD53BTools {

template <class Flavor>
void RD53BInjectionTool<Flavor>::init() {
    if (param("size"_s)[0] == 0)
        param("size"_s)[0] = RD53B<Flavor>::nRows - param("offset"_s)[0];
    if (param("size"_s)[1] == 0)
        param("size"_s)[1] = RD53B<Flavor>::nCols - param("offset"_s)[1];

    std::transform(param("injectionType"_s).begin(), param("injectionType"_s).end(), param("injectionType"_s).begin(), [] (const char& c) { return (char)std::tolower(c); });

    auto& steps = param("maskGen"_s);

    std::vector<size_t> sizes[2] = {{1}, {1}};
    std::vector<size_t> stepId[2];

    int i = 0;
    for (auto step : steps) {
        sizes[step["dim"_s]].push_back(step["size"_s]);
        stepId[step["dim"_s]].push_back(i);
        ++i;
    }

    for (int dim = 0; dim < 2; ++dim) {
        auto zeroPos = std::find(sizes[dim].begin(), sizes[dim].end(), 0);
        auto& zeroStep = steps[stepId[dim][zeroPos - sizes[dim].begin() - 1]];
        auto prev_size = std::accumulate(sizes[dim].begin(), zeroPos, 1lu, std::multiplies<>{});
        auto next_size = std::accumulate(sizes[dim].rbegin(), std::make_reverse_iterator(zeroPos + 1), param("size"_s)[dim], [=] (auto a, auto b) {
            return size_t(std::ceil(double(a) / b));
        });
        auto lcm = zeroStep["shift"_s].size() ? LCM(prev_size, zeroStep["shift"_s][0]) : prev_size;
        *zeroPos = size_t(std::ceil(double(next_size) / lcm)) * lcm / prev_size;
        std::partial_sum(sizes[dim].begin(), sizes[dim].end() - 1, sizes[dim].begin(), std::multiplies<>{});
        sizes[dim].back() = size_t(std::ceil(double(param("size"_s)[dim]) / *(sizes[dim].end() - 2))) * *(sizes[dim].end() - 2);
        for (auto it = sizes[dim].rbegin(); it != sizes[dim].rend() - 1; ++it)
            *it = *it / *(it + 1);
        for (size_t i = 1; i < sizes[dim].size(); ++i)
            steps[stepId[dim][i - 1]]["size"_s] = sizes[dim][i];
    }

    _nFrames = 1;
    for (const auto& step : steps) 
        if (!step["parallel"_s])
            _nFrames *= step["size"_s];
            
    _nFrames = std::ceil((double)_nFrames / param("frameStep"_s));

}

template <class Flavor>
typename RD53BInjectionTool<Flavor>::ChipEventsMap RD53BInjectionTool<Flavor>::run(Task progress) const {
    ChipEventsMap result;
    Base::for_each_chip([&] (auto chip) {
        result[chip].reserve(_nFrames * param("nInjections"_s) * param("triggerDuration"_s));
    });

    injectionScan(progress, std::array<ScanRange, 0>{}, [&, this] (size_t i, auto&& events) {
        Base::for_each_chip([&] (auto chip) {
            result[chip].insert(
                result[chip].end(),
                std::make_move_iterator(events[chip].begin()),
                std::make_move_iterator(events[chip].end())
            );
        });
    });

    return result;
}


template <class Flavor>
void RD53BInjectionTool<Flavor>::setupMaskFrame(size_t frameId) const {
    auto& chipInterface = Base::chipInterface();

    auto mask = generateInjectionMask(frameId);

    Base::for_each_chip([&] (auto* chip) {
        auto& pixelCfg = chip->pixelConfig();
        chipInterface.UpdatePixelMasks(
            chip, 
            param("disableUnusedPixels"_s) ? pixel_matrix_t<Flavor, bool>(mask && pixelCfg.enable) : pixelCfg.enable, 
            param("injectUnusedPixels"_s) ? pixelCfg.enableInjections : pixel_matrix_t<Flavor, bool>(mask && pixelCfg.enableInjections),
            pixelCfg.enableHitOr
        );
    });
}


template <class Flavor>
ChipDataMap<pixel_matrix_t<Flavor, double>> RD53BInjectionTool<Flavor>::occupancy(const ChipEventsMap& events) const {
    ChipDataMap<pixel_matrix_t<Flavor, double>> occ;
    for (const auto& item : events) {
        occ[item.first].fill(0);
        for (const auto& event : item.second)
            for (const auto& hit : event.hits)
                occ[item.first](hit.row, hit.col) += 1.0 / param("nInjections"_s);
    }
    return occ;
}

template <class Flavor>
ChipDataMap<std::array<size_t, 16>> RD53BInjectionTool<Flavor>::totDistribution(const ChipEventsMap& events) const {
    ChipDataMap<std::array<size_t, 16>> tot;
    auto used = usedPixels();
    Base::for_each_chip([&] (auto* chip) {
        auto it = tot.insert({chip, {0}}).first;
        for (const auto& event : events.at(chip)) {
            for (const auto& hit : event.hits) {
                ++it->second[hit.tot];
            }
        }
    });
    return tot;
}

template <class Flavor>
void RD53BInjectionTool<Flavor>::draw(const ChipEventsMap& result) {
    Base::createRootFile();
    
    auto occMap = occupancy(result);
    auto totMap = totDistribution(result);

    auto used = usedPixels();

    Base::for_each_chip([&] (RD53B<Flavor>* chip) {
        Base::createRootFileDirectory(chip);

        auto enabled = used && chip->injectablePixels();

        const auto& occ = occMap[chip];
        const auto& tot = totMap[chip];

        Base::drawHistRaw(tot, "ToT Distribution", 0, 16, "ToT", "Frequency");

        Base::drawMap(occ, "Occupancy Map", "Occupancy");

        LOG (INFO) 
            << "Number of enabled pixels: " << xt::count_nonzero(enabled)()
            << RESET;

        LOG (INFO) << "Mean occupancy for enabled pixels: " << xt::mean(xt::filter(occ, enabled))() << RESET;
        LOG (INFO) << "Max occupancy for enabled pixels: " << xt::amax(xt::filter(occ, enabled))() << RESET;
        LOG (INFO) << "Min occupancy for enabled pixels: " << xt::amin(xt::filter(occ, enabled))() << RESET;

        LOG (INFO) << "Mean occupancy for disabled pixels: " << xt::nan_to_num(xt::mean(xt::filter(occ, !enabled)))() << RESET;

        std::vector<size_t> triggerIdHist(param("triggerDuration"_s), 0);

        for (const auto& event : result.at(chip))
            triggerIdHist[event.triggerId % param("triggerDuration"_s)] += event.hits.size();

        Base::drawHistRaw(triggerIdHist, "Trigger ID Distribution", 0, param("triggerDuration"_s), "Trigger ID", "Count");

        
        std::vector<size_t> eventIdHist(param("nInjections"_s), 0);
        for (auto i = 0u; i < param("nInjections"_s); ++i)
            for (auto j = 0u; j < param("triggerDuration"_s); ++j)
                eventIdHist[i] += result.at(chip)[i * param("triggerDuration"_s) + j].hits.size();
        
        Base::drawHistRaw(eventIdHist, "Event ID Distribution", 0, param("nInjections"_s), "Event ID", "Count");
    });
}

template <class Flavor>
pixel_matrix_t<Flavor, bool> RD53BInjectionTool<Flavor>::generateInjectionMask(size_t frameId) const {
    pixel_matrix_t<Flavor, bool> fullMask;
    fullMask.fill(false);

    frameId *= param("frameStep"_s);

    xt::xtensor<bool, 2> mask = xt::ones<bool>({1, 1});
    size_t lastDim = 0;
    std::vector<size_t> currentShifts;

    for (auto step : param("maskGen"_s)) {
        auto size = step["size"_s];
        auto shape = mask.shape();
        
        std::array<size_t, 2> new_shape = shape;
        new_shape[step["dim"_s]] *= size;
        
        xt::xtensor<bool, 2> new_mask = xt::zeros<bool>(new_shape);

        size_t n = step["parallel"_s] ? size : 1;
        for (size_t i = 0; i < n; ++i) {
            size_t j = step["parallel"_s] ? i : frameId % size;
            size_t shift_count = i;
            size_t shift = 0;
            size_t wrapAroundSize = shape[lastDim]; 
            for (auto s : currentShifts) {
                shift += shift_count * s;
                shift_count = shift_count * s / wrapAroundSize;
                wrapAroundSize = s;
            }
            shift %= shape[lastDim];
            if (step["dim"_s] == 0) 
                xt::view(new_mask, xt::range(j * shape[0], (j + 1) * shape[0]), xt::all()) = xt::roll(mask, shift, lastDim);
            else
                xt::view(new_mask, xt::all(), xt::range(j * shape[1], (j + 1) * shape[1])) = xt::roll(mask, shift, lastDim);
        }

        if (!step["parallel"_s])
            frameId /= size;

        mask = new_mask;
        lastDim = step["dim"_s];
        currentShifts = step["shift"_s];
    }

    auto row_range = xt::range(param("offset"_s)[0], param("offset"_s)[0] + param("size"_s)[0]);
    auto col_range = xt::range(param("offset"_s)[1], param("offset"_s)[1] + param("size"_s)[1]);

    xt::view(fullMask, row_range, col_range) = xt::view(mask, xt::range(0, param("size"_s)[0]), xt::range(0, param("size"_s)[1]));

    return fullMask;
}

template <class Flavor>
void RD53BInjectionTool<Flavor>::configureInjections() const {
    auto& chipInterface = Base::chipInterface();
    Base::for_each_chip([&] (auto* chip) {
        chipInterface.WriteReg(chip, "LatencyConfig", param("triggerLatency"_s));
        chipInterface.WriteReg(chip, "DigitalInjectionEnable", param("injectionType"_s) == "digital");
        // chipInterface.WriteReg(chip, "EnOutputDataChipId", Base::system().template findValueInSettings<double>("eventReadoutChipID", 0));
        chipInterface.WriteReg(chip, "EnOutputDataChipId", param("enableChipIdReadout"_s));
        chipInterface.WriteReg(chip, "BinaryReadOut", !param("enableToTReadout"_s));
    });
    
    for (auto* board : *Base::system().fDetectorContainer) {
        auto& fwInterface = Base::getFWInterface(board);
        // auto& fastCmdConfig = *fwInterface.getLocalCfgFastCmd();
        RD53FWInterface::FastCommandsConfig fastCmdConfig;

        fastCmdConfig.n_triggers = param("nInjections"_s);
        fastCmdConfig.trigger_duration = param("triggerDuration"_s) - 1;

        fastCmdConfig.fast_cmd_fsm.first_cal_en = true;
        fastCmdConfig.fast_cmd_fsm.second_cal_en = true;
        fastCmdConfig.fast_cmd_fsm.trigger_en = true;

        fastCmdConfig.fast_cmd_fsm.delay_after_first_prime = 1000;
        fastCmdConfig.fast_cmd_fsm.delay_after_prime = param("delayAfterPrime"_s);
        fastCmdConfig.fast_cmd_fsm.delay_after_inject = param("delayAfterInject"_s);
        fastCmdConfig.fast_cmd_fsm.delay_after_trigger = param("delayAfterTrigger"_s);

        if (param("injectionType"_s) == "analog") {
            fastCmdConfig.fast_cmd_fsm.first_cal_data = bits::pack<1, 5, 8, 1, 5>(1, 0, 2, 0, 0);
            fastCmdConfig.fast_cmd_fsm.second_cal_data = bits::pack<1, 5, 8, 1, 5>(0, param("fineDelay"_s), param("pulseDuration"_s), 0, 0);
        }
        else if (param("injectionType"_s) == "digital") {
            // fastCmdConfig.fast_cmd_fsm.first_cal_en = false;
            fastCmdConfig.fast_cmd_fsm.first_cal_data = bits::pack<1, 5, 8, 1, 5>(1, 0, 2, 0, 0);
            fastCmdConfig.fast_cmd_fsm.second_cal_data = bits::pack<1, 5, 8, 1, 5>(1, param("fineDelay"_s), param("pulseDuration"_s), 0, 0);
        }
        fwInterface.ConfigureFastCommands(&fastCmdConfig);
    }
}


template <class Flavor>
pixel_matrix_t<Flavor, bool> RD53BInjectionTool<Flavor>::usedPixels() const {
    pixel_matrix_t<Flavor, bool> result;
    result.fill(0);
    for (size_t frameId = 0; frameId < nFrames() ; ++frameId) {
        result |= generateInjectionMask(frameId);
    }
    return result;
}

template class RD53BInjectionTool<RD53BFlavor::ATLAS>;
template class RD53BInjectionTool<RD53BFlavor::CMS>;

}
