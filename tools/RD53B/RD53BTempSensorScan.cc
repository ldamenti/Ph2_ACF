#include "RD53BTempSensorScan.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>

#include <iostream>

namespace RD53BTools {

template <class Flavor>
typename RD53BTempSensorScan<Flavor>::Result RD53BTempSensorScan<Flavor>::run(Task progress) const {
    auto& chipInterface = Base::chipInterface();

    Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

    dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    auto chip = Base::firstChip();

    std::map<std::string, SensorResults> results;

    static const std::map<std::string, bool> sensors = {
        {"RADSENS_SLDOA", true},
        {"TEMPSENS_SLDOA", true},
        {"RADSENS_SLDOD", true},
        {"TEMPSENS_SLDOD", true},
        {"RADSENS_ACB", false},
        {"TEMPSENS_ACB", false}
    };

    double nIterations = sensors.size() * 2 * 16;

    int i = 0;
    for (const auto& item : sensors) {
        auto& vmuxName = item.first;
        bool sldo = item.second;

        chipInterface.WriteReg(chip, "MonitorEnable", 1); //Choose VMUX entry
        chipInterface.WriteReg(chip, "VMonitor", Flavor::VMuxMap.at(vmuxName));

        for (int bias = 0; bias < 2; ++bias) {
            for(int sensorDEM=0; sensorDEM < 16; ++sensorDEM) {
                uint16_t sensorConfigData = bits::pack<1, 4, 1>(true, sensorDEM, bias);
                if (sldo) 
                    sensorConfigData = bits::pack<6, 6>(sensorConfigData, sensorConfigData);

                results[vmuxName].sensorConfigData[bias].push_back(sensorConfigData);

                if (sldo)
                    chipInterface.WriteReg(chip, "MON_SENS_SLDO", sensorConfigData);
                else
                    chipInterface.WriteReg(chip, "MON_SENS_ACB", sensorConfigData);

                results[vmuxName].voltages[bias].push_back(dKeithley2410.getVoltage());

                chipInterface.SendGlobalPulse(chip, {"ADCStartOfConversion"}); //ADC start conversion
                results[vmuxName].ADC[bias].push_back(chipInterface.ReadReg(chip, "MonitoringDataADC"));
                
                progress.update((i * 16 * 2 + bias * 16 + sensorDEM) / nIterations);
            }
        }
        ++i;
    }

    return results;
}

template <class Flavor>
void RD53BTempSensorScan<Flavor>::draw(const Result& results) const {
    using TeeDevice = boost::iostreams::tee_device<std::ostream, std::ostream>; 

    for (const auto& item: results) {
        LOG(INFO) << "Sensor: " << item.first;
        
        std::ofstream outFile(Base::getOutputFilePath(item.first + ".csv"));

        TeeDevice teeDevice(std::cout, outFile);
        boost::iostreams::stream<TeeDevice> tee(teeDevice);

        tee << "high bias voltage, low bias voltage, high bias adc, low bias adc, high bias config, low bias config\n";
       
        for (int i = 0; i < 16; ++i) {
            tee << item.second.voltages[0][i] << ", " 
                << item.second.voltages[1][i] << ", " 
                << item.second.ADC[0][i] << ", " 
                << item.second.ADC[1][i] << ", " 
                << std::bitset<12>(item.second.sensorConfigData[0][i]) << ", " 
                << std::bitset<12>(item.second.sensorConfigData[1][i]) << '\n';
        }
    }
    
}

template class RD53BTempSensorScan<RD53BFlavor::ATLAS>;
template class RD53BTempSensorScan<RD53BFlavor::CMS>;

}


