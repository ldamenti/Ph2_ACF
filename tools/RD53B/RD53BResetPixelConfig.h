#ifndef RD53BResetPixelConfig_H
#define RD53BResetPixelConfig_H

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
struct RD53BResetPixelConfig; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BResetPixelConfig<Flavor>> = make_named_tuple(
    std::make_pair("resetTdac"_s, false),
    std::make_pair("tdac"_s, 16u),
    std::make_pair("resetEnable"_s, false),
    std::make_pair("enable"_s, true),
    std::make_pair("resetEnableInjections"_s, false),
    std::make_pair("enableInjections"_s, 16u),
    std::make_pair("resetEnableHitOr"_s, false),
    std::make_pair("enableHitOr"_s, 16u)
);

template <class Flavor>
struct RD53BResetPixelConfig : public RD53BTool<RD53BResetPixelConfig, Flavor> {
    using Base = RD53BTool<RD53BResetPixelConfig, Flavor>;
    using Base::Base;
    using Base::param;

    bool run(Task progress) const {
        Base::for_each_chip([&] (auto chip) {
            if (param("resetTdac"_s))
                chip->pixelConfig().tdac.fill(param("tdac"_s));
            if (param("resetEnable"_s))
                chip->pixelConfig().enable.fill(param("enable"_s));
            if (param("resetEnableInjections"_s))
                chip->pixelConfig().enableInjections.fill(param("enableInjections"_s));
            if (param("resetEnableHitOr"_s))
                chip->pixelConfig().enableHitOr.fill(param("enableHitOr"_s));
            Base::chipInterface().UpdatePixelConfig(
                chip, 
                param("resetTdac"_s), 
                param("resetEnable"_s) || param("resetEnableInjections"_s) || param("resetEnableHitOr"_s)
            );
        });
        return true;
    }
};

}

#endif