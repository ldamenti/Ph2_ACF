#ifndef RD53BADCCalib_H
#define RD53BADCCalib_H

// #include <array>
// #include <cmath>
// #include <fstream>
// #include <sstream>
// #include <stdexcept>
// #include <string>
// #include <utility>
// #include <vector>

#include "RD53BTool.h"

namespace RD53BTools {

template <class>
class RD53BADCCalib;

template <class F>
const auto ToolParameters<RD53BADCCalib<F>> = make_named_tuple(
    std::make_pair("powerSupplyId"_s, std::string("analogCalib")),
    std::make_pair("powerSupplyChannelId"_s, std::string("analogCalib")),
    std::make_pair("minVoltage"_s, 0.0f),
    std::make_pair("maxVoltage"_s, 0.9f),
    std::make_pair("nVoltages"_s, 3),
    std::make_pair("adcSamples"_s, 2),
    std::make_pair("powerSupplySamples"_s, 2),
    std::make_pair("groundCorrection"_s, false),
    std::make_pair("voltmeterId"_s, std::string()),
    std::make_pair("voltmeterChannelId"_s, std::string()),
    std::make_pair("abortOnLimit"_s, false)
);

template <class F>
class RD53BADCCalib : public RD53BTool<RD53BADCCalib, F> {
public:
    struct Data {
        std::map<float, std::map<short, unsigned int>> adcData;
        std::array<std::vector<float>, 2> psData;
    };

    using Base = RD53BTool<RD53BADCCalib, F>;
    using Base::Base;
    using Base::param;
    using Results = ChipDataMap<Data>;

    void init();

    Results run();

    void draw(const Results& results);

private:
    void calibChip(Chip* chip, Data& res);

    float min, max;
    int nSamp, nVolt;
    std::string psId, psChId, psMsg;
};

}

#endif
