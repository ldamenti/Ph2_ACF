#ifndef BITSERIALIZATION__TYPES__CONDITIONAL_HPP
#define BITSERIALIZATION__TYPES__CONDITIONAL_HPP

#include "../Core.hpp"

namespace BitSerialization {
    
template <auto Condition, class TypeA, class TypeB=Void>
struct Conditional {
    static constexpr bool ignores_input_value = ignores_input_value<TypeA> && ignores_input_value<TypeB>;

    using value_type = std::variant<value_type_t<TypeA>, value_type_t<TypeB>>;

    using ParseError = ErrorVariant<parse_error_t<TypeA>, parse_error_t<TypeB>>; 

    template <class T, class U>
    static ParseResult<value_type, ParseError> 
    parse(const BitView<T>& bits, const U& parent) {
        if (Condition(parent)) {
            return do_parse<TypeA>(bits, parent);
        }
        else {
            return do_parse<TypeB>(bits, parent);
        }
    }

private:
    template <class Type, class T, class U>
    static ParseResult<value_type, ParseError>
    do_parse(const BitView<T>& bits, const U& parent) {
        auto result = Type::parse(bits, parent);
        if (result)
            return {std::move(result.value()), result.size()};
        else
            return {std::move(result.error())};
    }

public:
    using TypeError = Error<"Conditional: invalid type in variant during serialization">;

    using SerializeError = ErrorVariant<serialize_error_t<TypeA>, serialize_error_t<TypeB>, TypeError>; 

    template <class T, class U=Void>
    static SerializeResult<SerializeError>
    serialize(value_type& value, BitVector<T>& bits, const U& parent={}) {
        if (Condition(parent)) 
            return do_serialize<TypeA>(value);
        else 
            return do_serialize<TypeB>(value);
    }

private:
    template <class Type, class T, class U=Void>
    static SerializeResult<SerializeError>
    do_serialize(value_type& value, BitVector<T>& bits, const U& parent={}) {
        if (!std::holds_alternative<value_type_t<Type>>(value))
            return {TypeError{}};
        auto& v = std::get<value_type_t<Type>>(value.value());
        auto result = Type::serialize(v, bits, parent);
        if (!result)
            return {std::move(result.error())};
        else
            return {};
    }
};

}

#endif